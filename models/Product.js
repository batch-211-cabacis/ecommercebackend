const mongoose = require("mongoose");
			

const productSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Product name is required"]
		},
		image:{
				type: String
		}, 
		description: {
			type: String,
			required: [true, "Product description is required"]
		},
		price: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		orders: [{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}]
});

module.exports = mongoose.model("Product", productSchema);
