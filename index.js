const express = require("express");
const mongoose =require("mongoose");
const cors = require("cors");

const app = express();
const path = require("path");
require("dotenv/config");
const imgModel = require('./models/Product');


const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


const multer = require('multer');
 
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
 
const upload = multer({ storage: storage });


mongoose.connect("mongodb+srv://admin123:admin123@project0.jelgw36.mongodb.net/ecommercebackend?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open',()=>console.log("Now connected to MongoDB Atlas"));


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.set("view engine", "ejs");

app.use("/users",userRoutes);
app.use("/products",productRoutes);


app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});