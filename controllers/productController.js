const Product = require("../models/Product");


// Create/Add products

module.exports.addProduct = (data) =>{
	console.log(data);

	if(data.isAdmin){
		let newProduct = new Product({
		image: data.product.image,
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
			});

		return newProduct.save().then((product,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	});
	}else{
		return false;
	}	
};


// Retrieve all products

module.exports.getAllProducts = () =>{
	return Product.find({}).then(result=>{
		return result;
	})
};



// Retrieve all active products


module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
};


// Retrieve specific product

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	})
};


// Update a product

module.exports.updateProduct = (reqParams, reqBody)=>{
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
};


// Archiving a product

module.exports.archiveProduct = (reqParams, reqBody)=>{
		let archivedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};
		return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
};







