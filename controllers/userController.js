const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");



// Check if email exists


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email:reqBody.email}).then(
		result=> {
			if(result.length>0){
				return true;
			}else{
				return false;
			}
		})
};


// User registration

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		}
	})
};

// User Login/Authentication

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};

// Retrieving user details

module.exports.getProfile = (reqBody)=> {
	return User.findOne({id:reqBody.id}).then(result=>{
		if(result == null){
			return false;
		}
		result.password = "";
		return result;
});
};

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}



// Make user as admin

module.exports.updateUser = (reqParams, reqBody)=>{
		let updatedAdmin = {
			isAdmin: reqBody.isAdmin
		};
		return User.findByIdAndUpdate(reqParams.userId,updatedAdmin).then((user,error)=>{
			if(error){
				return false;
			}else{
				return "User is now admin.";
			};
		});
};



// Add order

module.exports.order = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.orders.push({productId:data.productId});
		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});
	let isProductUpdated = await Product.findById(data.productId).then(product=>{
		product.orders.push({userId:data.userId});
		return product.save().then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated){
		return true;
	}else{
		return false;
	}
};


