const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for adding/creating products

router.post("/",auth.verify,(req,res)=>{
	const data = {
		product: req.body,
		image: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))
});


// Retrieve all products

router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
});

// Retrieving all active products

router.get("/",(req,res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController));
});


// Retrieving a specific product

router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
});


// Update a product

router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// Route for archiving a product

router.put("/:productId/archive",auth.verify,(req,res)=>{
	productController.archiveProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});



// router.get('/', (req, res) => {
//     imgModel.find({}, (err, items) => {
//         if (err) {
//             console.log(err);
//             res.status(500).send('An error occurred', err);
//         }
//         else {
//             res.render('imagesPage', { items: items });
//         }
//     });
// });


// router.post('/', upload.single('image'), (req, res, next) => {
 
//     var obj = {
//         name: req.body.name,
//         desc: req.body.desc,
//         img: {
//             data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.file.filename)),
//             contentType: 'image/png'
//         }
//     }
//     imgModel.create(obj, (err, item) => {
//         if (err) {
//             console.log(err);
//         }
//         else {
//             // item.save();
//             res.redirect('/');
//         }
//     });
// });


module.exports = router;