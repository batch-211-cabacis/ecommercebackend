const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for checking user's email 

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(
		resultFromController=>res.send(resultFromController))
})



// Route for user registration

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})


// Route for Login/Authentication

router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
})


// Route for checking user details

router.post("/details", (req,res)=>{
	userController.getProfile(req.body).then(
		resultFromController=>res.send(resultFromController))
});

router.get("/details",auth.verify,(req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController))
});


// Route for making a user as admin

router.put("/:userId",(req,res)=>{
	userController.updateUser(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});

// Route to add order

router.post("/order",auth.verify,(req,res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.order(data).then(resultFromController=>res.send(resultFromController));
});



module.exports = router;
